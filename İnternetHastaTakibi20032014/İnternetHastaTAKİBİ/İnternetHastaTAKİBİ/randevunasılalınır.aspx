﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="randevunasılalınır.aspx.cs" Inherits="İnternetHastaTAKİBİ.randevunasılalınır" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">


        .auto-style1 {
            width: 100%;
            border: 1px solid #000000;
            background-color: #CCFFFF;
            height: 490px;
        }
        .auto-style3 {
            height: 109px;
        }
        .auto-style40 {
            width: 950px;
            height: 274px;
        }
        .auto-style42 {
            height: 342px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
    
    <table class="auto-style1">
        <tr>
            <td class="auto-style3">
                <img alt="banner" class="auto-style40" src="Images/img-home-banner.jpg" /></td>
        </tr>
        <tr>
            <td class="auto-style42">
                <div id="dlgRandevuNasilAlinir" class="ui-dialog ui-dialog-content ui-widget-content" style="position: relative; top: 0px; left: 0px; padding: 0.5em 1em; outline: 0px; border: 0px; background-image: none; color: rgb(34, 34, 34); display: block; overflow: auto; font-family: Arial, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; width: auto; min-height: 0px; max-height: none; height: 359px; background-position: initial initial; background-repeat: initial initial;">
                    <h2 style="border-bottom-width: 1px; border-bottom-style: dotted; border-bottom-color: rgb(211, 211, 211); color: rgb(193, 27, 49); font-size: 1.2em; font-weight: normal; margin: 10px 0px; padding-bottom: 2px;">Randevu Nasıl Alınır?</h2>
                    ALO182 aranarak;<ul>
                        <li>Ev, iş, ankesör ve cep telefonlarından Sağlık Bakanlığına bağlı ALO182 çağrı merkezi aranır.</li>
                        <li>ALO182 numaralı telefonu arayan vatandaş tarafından çağrıyı karşılayan asistana (canlı operatör) randevu talebinde bulunulan vatandaşın T.C. Kimlik Numarası verilir. Asistan vatandaş bilgilerini doğrular.</li>
                        <li>Vatandaş randevu talebinde bulunduğu hastane, poliklinik ve hekim bilgilerinden en azından hangi polikliniğe gideceğini asistana bildirdikten sonra, asistan uygun tarih ve saat dilimlerini vatandaşa iletir.</li>
                        <li>Vatandaş seçimini yaparak randevusunu alır.</li>
                    </ul>
                    Web&#39;den;<ul>
                        <li>Vatandaşlar, ALO182&#39;nin yanı sıra Online Randevu Sistemi aracılığıyla da randevu alabilmektedir.</li>
                        <li>Vatandaşlar, randevu almak istedikleri Hekim Randevu Sistemi kapsamında olan hastanelerin internet sitelerindeki Online Randevu linkine basarak, Vatandaş Giriş ekranından kayıt oluşturup randevu alabilmektedir.</li>
                    </ul>
                    <h2 style="border-bottom-width: 1px; border-bottom-style: dotted; border-bottom-color: rgb(211, 211, 211); color: rgb(193, 27, 49); font-size: 1.2em; font-weight: normal; margin: 10px 0px; padding-bottom: 2px;">Randevu Nasıl İptal Edilir?</h2>
                    ALO182 aranarak;<ul>
                        <li>Ev, iş, ankesör ve cep telefonlarından Sağlık Bakanlığına bağlı ALO182 çağrı merkezi aranır. Bu arama randevu zamanından en az 80 dakika önce yapılmalıdır.</li>
                        <li>ALO182 numaralı telefonu arayan vatandaş tarafından çağrıyı karşılayan asistana randevu talebinde bulunulan vatandaşın T.C. Kimlik Numarası verilir. Asistan vatandaş bilgilerini doğrular.</li>
                        <li>Vatandaş iptalini istediği randevusunun, tarih ve zamanını asistana iletir. Asistan tarafından ilgili randevu iptal edilir.</li>
                    </ul>
                    Web&#39;den;<ul>
                        <li>Hekim Randevu Merkezi Online Randevu ekranına T.C. Kimlik Numarası ve şifre ile giriş yapılmalıdır.</li>
                        <li>Randevu Geçmişi kısmına tıklayarak alınmış olunan tüm randevu bilgilerine erişilir ve Randevu iptal işlemi bu kısımdan yapılır.</li>
                        <li>Randevu iptali için Randevu iptal kısmına tıklayarak randevu iptal edilir.</li>
                    </ul>
                </div>
                <div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix" style="zoom: 1; min-height: 0px; border-width: 1px 0px 0px; border-style: solid; border-color: rgb(215, 215, 215); background-image: none; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); text-align: left; margin-top: 0.5em; padding: 0.1em; font-family: Arial, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: 50% 50%; background-repeat: repeat no-repeat;">
                    <div class="ui-dialog-buttonset" style="float: right;">
                        <button aria-disabled="false" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" style="display: inline-block; position: relative; padding: 0px; line-height: normal; margin: 0.5em 0.4em 0.5em 0px; cursor: pointer; vertical-align: middle; text-align: center; overflow: visible; text-decoration: none; font-family: Arial, sans-serif; font-size: 1em; border: 1px solid rgb(215, 215, 215); background-image: url(http://www.mhrs.gov.tr/Vatandas/themes/default/images/ui-bg_highlight-hard_75_d4d4d4_1x100.png); background-color: rgb(212, 212, 212); font-weight: normal; color: rgb(51, 51, 51); border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; background-position: 50% 50%; background-repeat: repeat no-repeat;" type="button">
                            <br class="Apple-interchange-newline" />
                        </button>
                    </div>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click1" Text="TAMAM" />
                </div>
            </td>
        </tr>
    </table>
    
    
    </form>
</body>
</html>
