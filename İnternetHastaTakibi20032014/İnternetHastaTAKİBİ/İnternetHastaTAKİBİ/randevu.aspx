﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="randevu.aspx.cs" Inherits="İnternetHastaTAKİBİ.randevu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            border-color: #66FFFF;
            background-color: #CCFFFF;
        }
        .auto-style3 {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">
                    <div id="inputBoxes2" style="width: 350px; color: rgb(0, 0, 0); font-family: Verdana; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(218, 238, 250);">
                        <div class="inputBox2" style="background-color: rgb(205, 237, 228); width: 350px; height: 35px; margin-bottom: 2px;">
                            <div class="inputBox2Left" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; width: 65px; float: left; padding-left: 10px; padding-top: 5px;">
                                İl</div>
                            <div class="inputBox2Right" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 51, 0); width: 275px; height: 35px; float: left;">
                                <input id="cmbIl" autocomplete="off" class="ui-corner-tr ui-combogrid-input" style="padding: 2px; margin-left: 0px; margin-right: 5px; border-top-right-radius: 10px; border-width: 1px; float: left; margin-top: 4px; height: 22px; font-weight: bold; width: 260px;" type="text" />
                                <button aria-disabled="false" class="ui-combogrid-trigger ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-tr" role="button" style="display: inline-block; position: relative; padding: 0px; margin-right: 0.1em; cursor: pointer; text-align: center; zoom: 1; overflow: hidden; text-decoration: none; width: auto; font-family: 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; font-size: 20px; border: 1px solid rgb(29, 174, 180); background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-bg_highlight-soft_60_439d84_1x100.png); background-color: rgb(67, 157, 132); font-weight: normal; color: rgb(255, 255, 255); border-top-right-radius: 10px; height: 28px; float: left; top: -28px; left: 246px; background-position: 50% 50%; background-repeat: repeat no-repeat;" tabindex="-1" title="">
                                    <span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s" style="display: block; text-indent: -99999px; overflow: hidden; width: 16px; height: 16px; background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-icons_f9a59f_256x240.png); position: absolute; top: 13px; margin-top: -8px; left: 8px; margin-left: -8px; background-position: -64px -16px; background-repeat: no-repeat no-repeat;"></span><span class="ui-button-text" style="display: block; line-height: 1.4; padding: 0.4em; text-indent: -9999999px;">&nbsp;</span>
                                </button>
                            </div>
                        </div>
                        <div class="inputBox2" style="background-color: rgb(205, 237, 228); width: 350px; height: 35px; margin-bottom: 2px;">
                            <div class="inputBox2Left" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; width: 65px; float: left; padding-left: 10px; padding-top: 5px;">
                                İlçe</div>
                            <div class="inputBox2Right" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 51, 0); width: 275px; height: 35px; float: left;">
                                <input id="cmbIlce" autocomplete="off" class="ui-corner-tr ui-combogrid-input" style="padding: 2px; margin-left: 0px; margin-right: 5px; border-top-right-radius: 10px; border-width: 1px; float: left; margin-top: 4px; height: 22px; font-weight: bold; width: 260px;" type="text" />
                                <button aria-disabled="false" class="ui-combogrid-trigger ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-tr" role="button" style="display: inline-block; position: relative; padding: 0px; margin-right: 0.1em; cursor: pointer; text-align: center; zoom: 1; overflow: hidden; text-decoration: none; width: auto; font-family: 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; font-size: 20px; border: 1px solid rgb(29, 174, 180); background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-bg_highlight-soft_60_439d84_1x100.png); background-color: rgb(67, 157, 132); font-weight: normal; color: rgb(255, 255, 255); border-top-right-radius: 10px; height: 28px; float: left; top: -28px; left: 246px; background-position: 50% 50%; background-repeat: repeat no-repeat;" tabindex="-1" title="">
                                    <span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s" style="display: block; text-indent: -99999px; overflow: hidden; width: 16px; height: 16px; background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-icons_f9a59f_256x240.png); position: absolute; top: 13px; margin-top: -8px; left: 8px; margin-left: -8px; background-position: -64px -16px; background-repeat: no-repeat no-repeat;"></span><span class="ui-button-text" style="display: block; line-height: 1.4; padding: 0.4em; text-indent: -9999999px;">&nbsp;</span>
                                </button>
                            </div>
                        </div>
                        <div class="inputBox2" style="background-color: rgb(205, 237, 228); width: 350px; height: 35px; margin-bottom: 2px;">
                            <div class="inputBox2Left" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; width: 65px; float: left; padding-left: 10px; padding-top: 5px;">
                                Hastane</div>
                            <div class="inputBox2Right" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 51, 0); width: 275px; height: 35px; float: left;">
                                <input id="cmbHastane" autocomplete="off" class="ui-corner-tr ui-combogrid-input" style="padding: 2px; margin-left: 0px; margin-right: 5px; border-top-right-radius: 10px; border-width: 1px; float: left; margin-top: 4px; height: 22px; font-weight: bold; width: 260px;" type="text" />
                                <button aria-disabled="false" class="ui-combogrid-trigger ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-tr" role="button" style="display: inline-block; position: relative; padding: 0px; margin-right: 0.1em; cursor: pointer; text-align: center; zoom: 1; overflow: hidden; text-decoration: none; width: auto; font-family: 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; font-size: 20px; border: 1px solid rgb(29, 174, 180); background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-bg_highlight-soft_60_439d84_1x100.png); background-color: rgb(67, 157, 132); font-weight: normal; color: rgb(255, 255, 255); border-top-right-radius: 10px; height: 28px; float: left; top: -28px; left: 246px; background-position: 50% 50%; background-repeat: repeat no-repeat;" tabindex="-1" title="">
                                    <span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s" style="display: block; text-indent: -99999px; overflow: hidden; width: 16px; height: 16px; background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-icons_f9a59f_256x240.png); position: absolute; top: 13px; margin-top: -8px; left: 8px; margin-left: -8px; background-position: -64px -16px; background-repeat: no-repeat no-repeat;"></span><span class="ui-button-text" style="display: block; line-height: 1.4; padding: 0.4em; text-indent: -9999999px;">&nbsp;</span>
                                </button>
                            </div>
                        </div>
                        <div class="inputBox2" style="background-color: rgb(205, 237, 228); width: 350px; height: 35px; margin-bottom: 2px;">
                            <div class="inputBox2Left" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; width: 65px; float: left; padding-left: 10px; padding-top: 0px;">
                                Semt<br />
                                Polikliniği</div>
                            <div class="inputBox2Right" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 51, 0); width: 275px; height: 35px; float: left;">
                                <input id="cmbHastaneSP" autocomplete="off" class="ui-corner-tr ui-combogrid-input" style="padding: 2px; margin-left: 0px; margin-right: 5px; border-top-right-radius: 10px; border-width: 1px; float: left; margin-top: 4px; height: 22px; font-weight: bold; width: 260px;" type="text" />
                                <button aria-disabled="false" class="ui-combogrid-trigger ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-tr" role="button" style="display: inline-block; position: relative; padding: 0px; margin-right: 0.1em; cursor: pointer; text-align: center; zoom: 1; overflow: hidden; text-decoration: none; width: auto; font-family: 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; font-size: 20px; border: 1px solid rgb(29, 174, 180); background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-bg_highlight-soft_60_439d84_1x100.png); background-color: rgb(67, 157, 132); font-weight: normal; color: rgb(255, 255, 255); border-top-right-radius: 10px; height: 28px; float: left; top: -28px; left: 246px; background-position: 50% 50%; background-repeat: repeat no-repeat;" tabindex="-1" title="">
                                    <span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s" style="display: block; text-indent: -99999px; overflow: hidden; width: 16px; height: 16px; background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-icons_f9a59f_256x240.png); position: absolute; top: 13px; margin-top: -8px; left: 8px; margin-left: -8px; background-position: -64px -16px; background-repeat: no-repeat no-repeat;"></span><span class="ui-button-text" style="display: block; line-height: 1.4; padding: 0.4em; text-indent: -9999999px;">&nbsp;</span>
                                </button>
                            </div>
                        </div>
                        <div class="inputBox2" style="background-color: rgb(205, 237, 228); width: 350px; height: 35px; margin-bottom: 2px;">
                            <div class="inputBox2Left" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; width: 65px; float: left; padding-left: 10px; padding-top: 5px;">
                                Klinik</div>
                            <div class="inputBox2Right" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 51, 0); width: 275px; height: 35px; float: left;">
                                <input id="cmbPoliklinik" autocomplete="off" class="ui-corner-tr ui-combogrid-input" style="padding: 2px; margin-left: 0px; margin-right: 5px; border-top-right-radius: 10px; border-width: 1px; float: left; margin-top: 4px; height: 22px; font-weight: bold; width: 260px;" type="text" />
                                <button aria-disabled="false" class="ui-combogrid-trigger ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-tr" role="button" style="display: inline-block; position: relative; padding: 0px; margin-right: 0.1em; cursor: pointer; text-align: center; zoom: 1; overflow: hidden; text-decoration: none; width: auto; font-family: 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; font-size: 20px; border: 1px solid rgb(29, 174, 180); background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-bg_highlight-soft_60_439d84_1x100.png); background-color: rgb(67, 157, 132); font-weight: normal; color: rgb(255, 255, 255); border-top-right-radius: 10px; height: 28px; float: left; top: -28px; left: 246px; background-position: 50% 50%; background-repeat: repeat no-repeat;" tabindex="-1" title="">
                                    <span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s" style="display: block; text-indent: -99999px; overflow: hidden; width: 16px; height: 16px; background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-icons_f9a59f_256x240.png); position: absolute; top: 13px; margin-top: -8px; left: 8px; margin-left: -8px; background-position: -64px -16px; background-repeat: no-repeat no-repeat;"></span><span class="ui-button-text" style="display: block; line-height: 1.4; padding: 0.4em; text-indent: -9999999px;">&nbsp;</span>
                                </button>
                            </div>
                        </div>
                        <div class="inputBox2" style="background-color: rgb(205, 237, 228); width: 350px; height: 35px; margin-bottom: 2px;">
                            <div class="inputBox2Left" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; width: 65px; float: left; padding-left: 10px; padding-top: 5px;">
                                Klinik Yeri</div>
                            <div class="inputBox2Right" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 51, 0); width: 275px; height: 35px; float: left;">
                                <input id="cmbPoliklinikYeri" autocomplete="off" class="ui-corner-tr ui-combogrid-input" style="padding: 2px; margin-left: 0px; margin-right: 5px; border-top-right-radius: 10px; border-width: 1px; float: left; margin-top: 4px; height: 22px; font-weight: bold; width: 260px;" type="text" />
                                <button aria-disabled="false" class="ui-combogrid-trigger ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-tr" role="button" style="display: inline-block; position: relative; padding: 0px; margin-right: 0.1em; cursor: pointer; text-align: center; zoom: 1; overflow: hidden; text-decoration: none; width: auto; font-family: 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; font-size: 20px; border: 1px solid rgb(29, 174, 180); background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-bg_highlight-soft_60_439d84_1x100.png); background-color: rgb(67, 157, 132); font-weight: normal; color: rgb(255, 255, 255); border-top-right-radius: 10px; height: 28px; float: left; top: -28px; left: 246px; background-position: 50% 50%; background-repeat: repeat no-repeat;" tabindex="-1" title="">
                                    <span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s" style="display: block; text-indent: -99999px; overflow: hidden; width: 16px; height: 16px; background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-icons_f9a59f_256x240.png); position: absolute; top: 13px; margin-top: -8px; left: 8px; margin-left: -8px; background-position: -64px -16px; background-repeat: no-repeat no-repeat;"></span><span class="ui-button-text" style="display: block; line-height: 1.4; padding: 0.4em; text-indent: -9999999px;">&nbsp;</span>
                                </button>
                            </div>
                        </div>
                        <div class="inputBox2" style="background-color: rgb(205, 237, 228); width: 350px; height: 35px; margin-bottom: 2px;">
                            <div class="inputBox2Left" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; width: 65px; float: left; padding-left: 10px; padding-top: 5px;">
                                Hekim</div>
                            <div class="inputBox2Right" style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 51, 0); width: 275px; height: 35px; float: left;">
                                <input id="cmbHekim" autocomplete="off" class="ui-corner-tr ui-combogrid-input" style="padding: 2px; margin-left: 0px; margin-right: 5px; border-top-right-radius: 10px; border-width: 1px; float: left; margin-top: 4px; height: 22px; font-weight: bold; width: 260px;" type="text" />
                                <button aria-disabled="false" class="ui-combogrid-trigger ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-tr" role="button" style="display: inline-block; position: relative; padding: 0px; margin-right: 0.1em; cursor: pointer; text-align: center; zoom: 1; overflow: hidden; text-decoration: none; width: auto; font-family: 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; font-size: 20px; border: 1px solid rgb(29, 174, 180); background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-bg_highlight-soft_60_439d84_1x100.png); background-color: rgb(67, 157, 132); font-weight: normal; color: rgb(255, 255, 255); border-top-right-radius: 10px; height: 28px; float: left; top: -28px; left: 246px; background-position: 50% 50%; background-repeat: repeat no-repeat;" tabindex="-1" title="">
                                    <span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s" style="display: block; text-indent: -99999px; overflow: hidden; width: 16px; height: 16px; background-image: url(http://www.hastanerandevu.gov.tr/Randevu/src_css/tema/sb/images/ui-icons_f9a59f_256x240.png); position: absolute; top: 13px; margin-top: -8px; left: 8px; margin-left: -8px; background-position: -64px -16px; background-repeat: no-repeat no-repeat;"></span><span class="ui-button-text" style="display: block; line-height: 1.4; padding: 0.4em; text-indent: -9999999px;">&nbsp;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(218, 238, 250);">
                        <div class="gributon" style="float: left; display: inline; margin-left: 20px; width: 234px;">
                            <asp:Button ID="Button1" runat="server" ForeColor="Red" Text="Randevu  Ara" Width="220px" OnClick="Button1_Click" />
                        </div>
                        <br />
                        <br />
                    </div>
                </td>
                <td rowspan="3"><span style="color: rgb(0, 0, 0); font-family: Verdana; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(218, 238, 250); font-size: 24px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: gray; display: block;">Hoş Geldiniz</span><br style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(218, 238, 250);" />
                    <b style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(218, 238, 250);">Randevunuzu<span class="Apple-converted-space">&nbsp;</span><span class="randevuAdim" style="font-size: 26px; color: red; font-weight: normal;">3</span><span class="Apple-converted-space">&nbsp;</span>adımda alabileceksiniz.</b><br style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(218, 238, 250);" />
                    <blockquote style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(218, 238, 250);">
                        <span class="randevuAdim" style="font-size: 26px; color: red; font-weight: normal;">1.</span><span class="Apple-converted-space">&nbsp;</span>adımda sol taraftaki arama araçlarını kullanarak uygun hekimlerin listesini getirebilirsiniz.<br />
                        <span class="randevuAdim" style="font-size: 26px; color: red; font-weight: normal;">2.</span><span class="Apple-converted-space">&nbsp;</span>adımda hekim listesinden istediğiniz hekimi seçerek hekim çalışma cetvelini görüntüleyebilirsiniz.<br />
                        <span class="randevuAdim" style="font-size: 26px; color: red; font-weight: normal;">3.</span><span class="Apple-converted-space">&nbsp;</span>adımda hekim çalışma cetvelinden uygun bir slot seçip randevunuzu kaydedebilirsiniz.</blockquote>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">
                        <asp:Button ID="Button2" runat="server" ForeColor="Red" OnClick="Button2_Click" Text="Randevu Nasıl İptal  Edilir ?" />
                </td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Button ID="Button3" runat="server" ForeColor="Red" OnClick="Button3_Click1" Text="Randevu Nasıl  Alınır ?" Width="237px" />
                </td>
            </tr>
            <tr>
                <td class="auto-style3" colspan="2"><span style="color: rgb(0, 0, 0); font-family: Verdana; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(218, 238, 250); clear: both; position: absolute; margin-top: 8px; font-size: 11px; margin-left: 220px;">Soru ve sorunlarınız için<span class="Apple-converted-space">&nbsp;</span><a href="mailto:mhrs@saglik.gov.tr" style="text-decoration: none; color: navy; font-weight: bold;">mhrs@saglik.gov.tr</a><span class="Apple-converted-space">&nbsp;</span>adresimizi kullanarak bize ulaşabilirsiniz.</span></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
