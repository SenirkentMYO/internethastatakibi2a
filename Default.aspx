﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs"   Inherits="_14032014hastatakibi.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css"; background-color="#339999">
        .auto-style1 {
            border-color: #008080;
            background-color: #339999;
        }
        .auto-style2 {            height: 166px;
        }
        .auto-style8 {
            width: 225px;
            }
        .auto-style14 {
            height: 76px;
        }
        .auto-style15 {
            width: 125px;
            height: 76px;
        }
        .auto-style16 {
            width: 950px;
            height: 274px;
        }
        .auto-style17 {
            border: 1px solid #339999;
            background-color: #339999;
            width: 314px;
        }
        .auto-style26 {
            height: 80px;
        }
        .auto-style27 {
            width: 125px;
            }
        .auto-style32 {
            width: 323px;
            height: 35px;
        }
        .auto-style33 {
            width: 341px;
            height: 35px;
        }
        #Select1 {
            width: 157px;
        }
        #Select2 {
            width: 157px;
        }
        .auto-style34 {
            width: 100%;
        }
        .auto-style35 {
            width: 323px;
            height: 73px;
        }
        .auto-style36 {
            width: 341px;
            height: 73px;
        }
        .auto-style37 {
            height: 80px;
            width: 148px;
        }
        .auto-style38 {
            height: 76px;
            width: 148px;
        }
        .auto-style39 {
            width: 320px;
            height: 320px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table align="center" class="auto-style1">
            <tr>
                <td class="auto-style2" colspan="4">
                    <img alt="banner" class="auto-style16" src="image/img-home-banner.jpg" /></td>
            </tr>
            <tr>
                <td class="auto-style8" rowspan="2">
                    <img alt="kayit" class="auto-style39" src="image/WHO_doctor-320x320.jpg" /></td>
                <td class="auto-style37">
                    <table  class="auto-style17"  align="left">
                        <tr>
                            <td class="auto-style32">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#E3B9A1" Text="TC Kimlik No:"></asp:Label>
                            </td>
                            <td class="auto-style33">
                                <asp:TextBox ID="TextBox1" runat="server" Font-Bold="True" Font-Italic="False" ForeColor="#339999"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style32">
                                <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="#E3B9A1" Text="Ad-Soyad:"></asp:Label>
                            </td>
                            <td class="auto-style33">
                                <asp:TextBox ID="TextBox2" runat="server" Width="151px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style32">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="#E3B9A1" Text="Cinsiyet:"></asp:Label>
                            </td>
                            <td class="auto-style33">
                                <select id="Select1" itemtype="Web.config" name="D1">
                                    <option></option>
                                </select></td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style26">
                    <table  class="auto-style17"  align="left">
                        <tr>
                            <td class="auto-style32">
                                <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="#E3B9A1" Text="Kat Numarası:"></asp:Label>
                            </td>
                            <td class="auto-style33">
                                <asp:TextBox ID="TextBox3" runat="server" Font-Bold="True" Font-Italic="False" ForeColor="#339999"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style32">
                                <asp:Label ID="Label5" runat="server" Font-Bold="True" ForeColor="#E3B9A1" Text="Oda Numarası:"></asp:Label>
                            </td>
                            <td class="auto-style33">
                                <asp:TextBox ID="TextBox4" runat="server" Width="151px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style35">
                                <asp:Label ID="Label6" runat="server" Font-Bold="True" ForeColor="#E3B9A1" Text="Hasta İsmi:"></asp:Label>
                            </td>
                            <td class="auto-style36">
                                <table class="auto-style34"  align="left">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="Button1" runat="server" Text="Hasta Ara" Width="155px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style27" rowspan="2"></td>
            </tr>
            <tr>
                <td class="auto-style38"></td>
                <td class="auto-style14"></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
